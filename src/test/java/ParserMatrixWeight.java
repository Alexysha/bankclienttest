import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Баскаков Алексей
 * Парсер exel дока
 */
public class ParserMatrixWeight {
    private static int markers, segments;

    public static double[][] getMatrix() {
        double[][] matrix;

        XSSFWorkbook xssfSheets = null; //Путь к таблице

        try {
            xssfSheets = new XSSFWorkbook(new FileInputStream("book.xlsx"));
            readConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }

        XSSFSheet xssfSheet = xssfSheets.getSheet("Лист1"); // Название листа

        matrix = new double[markers][segments]; // Узнаем размер матрицы

        for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
            XSSFRow row = xssfSheet.getRow(i); // Парсим строку
            for (int j = 1; j < row.getLastCellNum(); j++) {
                matrix[i - 1][j - 1] =
                        Double.parseDouble(row.getCell(j).toString()); // Задаем значение матрице
            }
        }

        return matrix;
    }

    private static void readConfig() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("config.properties");
        Properties p = new Properties();
        p.load(fileInputStream);

        markers = Integer.parseInt(p.getProperty("NUMBER_OF_MARKERS"));
        segments = Integer.parseInt(p.getProperty("NUMBER_OF_SEGMENTS"));
    }
}
