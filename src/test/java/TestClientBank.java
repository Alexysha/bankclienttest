import ModelObject.*;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;

@Epic(value = "Тестирование банковской сисемы")
@Owner("Баскаков Алексей Андреевич")
@Severity(value = SeverityLevel.CRITICAL)
public class TestClientBank {
    private Clients clients;
    private Markers markers;
    private Statements statements;
    private HashMap<Integer, Client> clientsMap;
    private MyCriteria myCriteria;

    @BeforeClass
    public void setUpClass() throws JAXBException {
        clients = (Clients) fromXmlToObject(Clients.class, "xml/clients.xml");
        markers = (Markers) fromXmlToObject(Markers.class, "xml/markers.xml");
        statements = (Statements) fromXmlToObject(Statements.class, "xml/statements.xml");
        myCriteria = new MyCriteria(ParserMatrixWeight.getMatrix(), clients.getClient());
        clientsMap = new HashMap<>();

        fillHashMap();
    }

    @Feature("Тестирование очереди обращений")
    @Story("Проверка граничных значений")
    @Test
    public void testLowerAndUpperValue() {
        for (Statement s : statements.getStatements()) {
            int rank = myCriteria.getRank(s);

            Assert.assertTrue(rank >= 0); // Проверка нижней границы
            Assert.assertTrue(rank <= myCriteria.getQueueStatement().size()); // Проверка верхней границы
        }
    }

    @Feature("Тестирование очереди обращений")
    @Story("Проверка что клиенты из сегмента MASSIVE, не опускаются в очереди более чем на 5 позиций ниже начальной")
    @Test
    public void testStartingPosition() {
        int rank = -1;
        Client client;
        Statement statement = null;

        for (Statement s : statements.getStatements()) {
            client = clientsMap.get(s.getClientID()); // Находим клиента по обращению
            // Узнаем у кокого обращения, есть клиент с массовым сегментом
            // А также делаем это один раз за весь цикл
            if (rank == -1 && client.getSegments() == Segment.MASSIVE) {
                statement = s; // Сохраняем обращение
                rank = myCriteria.getRank(s); // Сохраняем позицию в очереди
                continue;
            }
            myCriteria.getRank(s);
        }
        // Проверяем чтобы позиция не увеличилась более чем на 5 позиций
        Assert.assertTrue(myCriteria.getRank(statement) <= rank + 5);
    }

    @Feature("Тестирование очереди обращений")
    @Story("Проверка клиента с двумя маркерами (приоритет).")
    @Test
    public void testDistributionVIP() {
        Statement statement = statements.getStatements().get(2);
        int rank = myCriteria.getRank(statement);

        Assert.assertEquals(rank, 14); // ранг элемента должен быть = 14
    }

    @Step("Конвертирование объекта в xml-файл")
    private void convertObjectToXml(Class classObj, Object[] obj, String filePath) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(classObj);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(obj, new File(filePath));
    }

    @Step("Конвертирование xml-файла в объект")
    private Object fromXmlToObject(Class classObj, String filePath) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(classObj);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return unmarshaller.unmarshal(new File(filePath));
    }

    @Step("Заполнение хэш мапа значениями")
    private void fillHashMap() {
        for (Client c : clients.getClient())
            clientsMap.put(c.getId(), c);
    }

    @AfterClass
    public void tearDownClass() {
    }
}