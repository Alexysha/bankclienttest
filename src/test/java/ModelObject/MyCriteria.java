package ModelObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyCriteria implements Criteria {
    private double[][] matrixWeight;
    private List<Client> clients;
    private Collection<Statement> queueStatement;

    public MyCriteria(double[][] matrixWeight, List<Client> clients) {
        this.matrixWeight = matrixWeight;
        this.clients = clients;
        queueStatement = new ArrayList<>();
    }

    public Collection<Statement> getQueueStatement() {
        return queueStatement;
    }

    @Override
    public int getRank(Statement statement) {
        return 0;
    }
}
