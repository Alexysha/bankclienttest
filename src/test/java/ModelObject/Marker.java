package ModelObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Marker")
public class Marker {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
