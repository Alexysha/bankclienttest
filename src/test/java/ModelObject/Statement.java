package ModelObject;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Statement")
public class Statement {

    @XmlAttribute
    private Integer clientID;
    private String textStatement;
    private Boolean promptly;
    private Boolean publicResonance;

    public Statement() {
        clientID = -1;
    }

    public String getTextStatement() {
        return textStatement;
    }

    public void setTextStatement(String textStatement) {
        this.textStatement = textStatement;
    }

    public boolean isPromptly() {
        return promptly;
    }

    public void setPromptly(Boolean promptly) {
        this.promptly = promptly;
    }

    public boolean isPublicResonance() {
        return publicResonance;
    }

    public void setPublicResonance(Boolean publicResonance) {
        this.publicResonance = publicResonance;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(Integer clientID) {
        this.clientID = clientID;
    }
}


