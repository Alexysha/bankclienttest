package ModelObject;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "Client")
public class Client {

    @XmlAttribute
    private Integer id;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String dateOfBirth;
    private Segment segments;
    private List<String> documentsID;
    private List<String> markers;

    public Client() {
        id = -1;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Segment getSegments() {
        return segments;
    }

    public void setSegments(Segment segments) {
        this.segments = segments;
    }

    public List<String> getDocumentsID() {
        return documentsID;
    }

    public void setDocumentsID(List<String> documentsID) {
        this.documentsID = documentsID;
    }

    public List<String> getMarkers() {
        return markers;
    }

    public void setMarkers(List<String> markers) {
        this.markers = markers;
    }

    @XmlTransient
    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
